#include "../inc/PlayerSnake.hpp"

PlayerSnake::PlayerSnake(int height, int startLength, int resolution) : Snake(height, startLength, resolution)
{
    prg::application.addKeyListener( *this );
    colour_ = prg::Colour::WHITE;
}

PlayerSnake::~PlayerSnake()
{
    prg::application.removeKeyListener( *this );
}

bool PlayerSnake::onKey(const KeyEvent& key_event)
{
    //set diection based on w, a, s, d keys
    if( key_event.key_state == KeyEvent::KB_DOWN )
    {
        switch( key_event.key )
        {
        case 'w':
            if( direction_ != DOWN)
                direction_ = UP;
            break;
        case 'd':
            if ( direction_ != LEFT )
                direction_ = RIGHT;
            break;
        case 's':
            if( direction_ != UP )
                direction_ = DOWN;
            break;
        case 'a':
            if( direction_ != RIGHT )
                direction_ = LEFT;
            break;
        }
    }
    return true;
}

void PlayerSnake::update(bool deleteTail)
{
    //Get the head of the snake
    SnakeSegment s = segments[0];

    //detect the Key Press
    if( direction_ == UP)
    {
        s.setY( s.getY() + resolution_);
    }
    else if( direction_ == RIGHT  )
    {
        s.setX( s.getX() + resolution_);
    }
    else if( direction_ == DOWN )
    {
        s.setY( s.getY() - resolution_);
    }
    else if( direction_ == LEFT )
    {
        s.setX( s.getX() - resolution_);
    }



    //create a new segment at the new position
    SnakeSegment n = SnakeSegment(s.getX(), s.getY());

    //make the new segment the head of the snake
    segments.push_front(n);

    //remove the end of the new snake to keep the same length
    if(deleteTail)
        segments.pop_back();
}

