#include "../inc/PausedState.hpp"
const char* pausedTitle = "Paused";
const char* pausedMessage = "press space to resume";

PausedState::PausedState()
{
    //create a new snake
    demoSnake = new AISnake(20, 5, 20);
}

bool PausedState::onCreate()
{
    return true;
}

bool PausedState::onDestroy()
{
    delete demoSnake;
    return true;
}

void PausedState::onEntry()
{
    prg::application.addKeyListener( *this );
    demoSnakeTimer_.start();
}

void PausedState::onExit()
{
    prg::application.removeKeyListener( *this );
    demoSnakeTimer_.stop();
}

void PausedState::onUpdate()
{

}

void PausedState::onRender(prg::Canvas& canvas)
{
    demoSnake->render( canvas );
    prg::uint dimensions[2];
    pausedFont_.computePrintDimensions(
        dimensions,
        pausedTitle
    );

    prg::uint   center_x { prg::application.getScreenWidth() / 2 - dimensions[0] / 2 },
        center_y { prg::application.getScreenHeight() / 2 - dimensions[1] / 2 };

    pausedFont_.print(
        canvas,
        center_x,
        center_y + 75,
        prg::Colour::WHITE,
        pausedTitle
    );
    pausedFont_.print(
        canvas,
        center_x - 110,
        center_y - 25,
        prg::Colour::WHITE,
        pausedMessage
    );
}

void PausedState::onTimer( prg::Timer& timer )
{
    demoSnake->update(true);
}

bool PausedState::onKey( const KeyEvent& key_event )
{
    switch(key_event.key_state)
    {
    case KeyEvent::KB_DOWN:
        switch( key_event.key )
        {
        case 27://escape key
            prg::application.exit();
            break;
        case 32://space bar key
            resumeGame();
            break;

        }
        break;
    }
    return true;
}
void PausedState::resumeGame()
{
    prg::application.setState("main_state");
}
