#include "../inc/Input.hpp"
#include "../inc/MainState.hpp"
#include "../inc/main.hpp"
#include <stdlib.h>
#include <time.h>
#include <list>
#include "../inc/FoodType.hpp"

using namespace std;

bool MainState::onCreate()
{
    //storing the last time the update was ran
    lastUpdatedTime = prg::application.upTime();

    //seed the random number generator
    srand (time(0));

    //create all the food types
    foodTypes.push_back(FoodType(prg::Colour::RED, 1, 10, false));
    foodTypes.push_back(FoodType(prg::Colour::GREEN, 5, 6, true));
    foodTypes.push_back(FoodType(prg::Colour::YELLOW, 10, 4, false));

    //create the snakes
    playerSnake = new PlayerSnake(600, 5, 20);
    aiSnake = new AISnake(20, 5, 20);

    //work out the board size
    xmax = (WIDTH - playerSnake->getResolution()/2) / 10;
    ymax = (HEIGHT - playerSnake->getResolution()/2) / 10;
    xmin = playerSnake->getResolution()/20;
    ymin = xmin;

    //add the starting foods
    foods.clear();
    for(int i = 0; i < 5; i++)
    {
        addFood();
    }
    return true;
}

bool MainState::onDestroy()
{
    //clean up
    delete aiSnake;
    delete playerSnake;
    return true;
}

void MainState::onEntry()
{
    prg::application.addKeyListener( *this );
    //start aiSnake
    aiSnakeTimer_.start();
}

void MainState::onExit()
{
    prg::application.removeKeyListener( *this );
    //stop aiSnake
    aiSnakeTimer_.stop();
}

void MainState::onUpdate()
{
    //get the current time
    long double currentTime = prg::application.upTime();

    //slows down the update loop depending on how long since the last update
    if(currentTime - lastUpdatedTime > 35000000)
    {
        //update snake position
        playerSnake->update(true);
        if( playerSnake->hasHitWall())
        {
            prg::logger.info( "Player has hit wall");
            gameOver();
        }
        if(playerSnake->hasHitSelf())
        {
            gameOver();
        }

        //check to see if the snake has hit any food
        for(auto it = foods.begin(); it != foods.end();)
        {
            Food f = (Food)(*it);
            if(playerSnake->hasCollided(f) == true)
            {
                //remove food from foods
                it = foods.erase(it);
                //update the snake
                playerSnake->update(false);
                //add correct number of points to the player score
                playerScore += f.getFoodType().getFoodPoints();
                //check to see if flake required
                if(f.getFoodType().getRemoveTail() == true)
                    createFlake(playerSnake);
                //add a new food to foods
                addFood();

            }
            else if(f.update() == true)//check if food been there too long
            {
                it = foods.erase(it);
                addFood();
            }
            else
                ++it;
        }
        if(playerSnake->hasCollided(aiSnake))
            gameOver();

        //check for collision with any flakes
        for(unsigned int i = 0; i < flakes.size(); i++)
        {
            if(playerSnake->hasCollided(flakes[i]))
                gameOver();
        }
        //update the last update time
        lastUpdatedTime = prg::application.upTime();
    }

}


void MainState::onRender(prg::Canvas& canvas)
{
    //draw the snakes
    playerSnake->render( canvas );
    aiSnake->render( canvas );

    //draw the flakes
    for(unsigned int i = 0; i < flakes.size(); i++)
    {
        flakes[i].render( canvas );
    }

    //draw the foods
    std::list<Food>::iterator it;
    for(it = foods.begin(); it != foods.end(); it++)
    {
        (*it).render(canvas);
    }

    //update the player score on the screen
    prg::Font::MEDIUM.print(
        canvas,
        670, 5,
        prg::Colour::WHITE,
        "Score:");

    prg::Font::MEDIUM.print(
        canvas,
        750, 5,
        prg::Colour::WHITE,
        convertInt(playerScore));
}

//handles the timer event
//I wanted to try driving the snake from a timer rather than just using the onUpdate function
void MainState::onTimer( prg::Timer& timer )
{
    //update the snake position
    aiSnake->update(true);

    //check to see if the snake has hit any food
    for(auto it = foods.begin(); it != foods.end();)
    {
        Food f = (Food)(*it);
        if(aiSnake->hasCollided(f) == true)
        {
            //remove food from foods
            it = foods.erase(it);
            //update the snake
            aiSnake->update(false);
            //increase the aiScore
            aiScore += f.getFoodType().getFoodPoints();
            //check to see if flake required
            if(f.getFoodType().getRemoveTail() == true)
                createFlake(aiSnake);
            //add new food to foods
            addFood();
            //make the snake look for different food
            break;
        }
        ++it;
    }

    //work out which the closest food is to the aiSnake
    Food closestFood;
    float closestDistance = 1000.0f;
    float currentDistance = 0.0f;

    for( auto it = foods.begin(); it != foods.end();)
    {
        Food f = (Food)(*it);
        currentDistance = foodDistance(aiSnake, f );

        if( currentDistance < closestDistance )
        {
            closestDistance = currentDistance;
            closestFood = f;
        }
        ++it;
    }
    if(aiSnake->hasCollided(playerSnake))
        gameOver();
    if(aiSnake->hasHitSelf())
    {
        gameOver();
    }
    for(unsigned int i = 0; i < flakes.size(); i++)
    {
        if(aiSnake->hasCollided(flakes[i]))
            gameOver();
    }
    //only work out the direction on every fourth update cycle
    //otherwise the aiSnake is too good
    if(updateCount == 4)
    {
        SnakeDirection d = calculateDirection(aiSnake, closestFood);

        aiSnake->setDirection(d);
        updateCount = 0;
    }
    updateCount++;
}

void MainState::pauseGame()
{
    aiSnakeTimer_.stop();
    prg::application.setState("paused_state");
}


bool MainState::onKey( const KeyEvent& key_event )
{
    switch(key_event.key_state)
    {
    case KeyEvent::KB_DOWN:
        switch( key_event.key )
        {
        case 27://escape key
            prg::application.exit();
            break;
        case 32://space bar key
            pauseGame();
            break;

        }
        break;
    }
    return true;
}

MainState::MainState()
{

}

void MainState::gameOver()
{
    //store scores in the global variables
    pScore = playerScore;
    aScore = aiScore;
    //reset the game
    playerSnake->reset();
    aiSnake->reset();
    playerScore = 0;
    aiScore = 0;
    flakes.clear();
    //change the state
    prg::application.setState("game_over_state");
}

int MainState::randomEvenNumber( int minimum, int maximum )
{
    return 2 * ( rand() % maximum/2 + minimum);
}

int MainState::randomOddNumber(int minimum, int maximum)
{
    return 1 + randomEvenNumber(minimum, maximum);
}

int MainState::randomNumber(int minimum, int maximum)
{
    return rand() % ( maximum + minimum );
}

void MainState::addFood()
{
    int x = randomOddNumber(xmin, xmax) * 10;
    int y = randomOddNumber(ymin, ymax) * 10;
    Food f = Food(x,y, 7.5f, foodTypes.at(randomNumber( 0, 3 )));
    foods.push_back(f);
}

string MainState::convertInt(int number)
{
    stringstream ss;//create a stringstream
    ss << number;//add number to the stream
    return ss.str();//return a string with the contents of the stream
}

//use pythagoras theorem to work out the distance from the snake head to a food
float MainState::foodDistance(Snake* s, Food f)
{
    float distance_ = sqrt((pow((s->segments[0].getX() - f.getX()),2))+(pow((s->segments[0].getY() - f.getY()),2)));
    return distance_;
}

//calculate the direction based on the x and y differentials between snake and food
SnakeDirection MainState::calculateDirection(Snake* s, Food f)
{
    int xdiff = (s->segments[0].getX() - f.getX());
    int ydiff = (s->segments[0].getY() - f.getY());

    if( abs(xdiff) >= abs(ydiff))
    {
        if( xdiff > 0 )
            return LEFT;
        return RIGHT;
    }
    else
    {
        if( ydiff > 0)
            return DOWN;
        return UP;
    }
}

void MainState::createFlake(Snake* snake)
{
    int x = snake->segments.back().getX();
    int y = snake->segments.back().getY();
    SnakeSegment s = SnakeSegment(x,y);
    flakes.push_back(s);
}
