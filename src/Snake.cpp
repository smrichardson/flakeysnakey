#include "../inc/Snake.hpp"
#include "../inc/SnakeDirection.hpp"
#include"../inc/main.hpp"

Snake::Snake()
{

}

Snake::Snake(int height, int startLength, int resolution)
{
    direction_ = RIGHT;
    height_ = height;

    startLength_ = startLength;
    resolution_ = resolution;

    int startx = startLength*resolution + resolution/2;
    int starty = height - resolution/2;

    headX_ = startx;
    headY_ = starty;

    for(int i=0; i< startLength; i++)
    {
        SnakeSegment segment = SnakeSegment(startx -(resolution * i), starty);
        segments.push_back(segment);
    }
}


void Snake::render(prg::Canvas& canvas)
{
    for(unsigned int i = 0; i < segments.size(); i++)
    {
        int x = ((SnakeSegment)segments[i]).getX();
        int y = ((SnakeSegment)segments[i]).getY();
        canvas.drawCircle(x, y, resolution_/2, colour_);
    }
}

void Snake::reset()
{
    int startx = startLength_*resolution_ + resolution_/2;
    int starty = height_ - resolution_/2;
    startx = headX_;
    starty = headY_;
    segments.clear();
    for(int i=0; i< startLength_; i++)
    {
        SnakeSegment segment = SnakeSegment(startx -(resolution_ * i), starty);
        segments.push_back(segment);
    }
    direction_ = RIGHT;
}

bool Snake::hasCollided(Snake* otherSnake)
{
    for(unsigned int i = 1; i < otherSnake->segments.size(); i++ )
    {
        if( (segments[0].getX() == otherSnake->segments[i].getX())&&(segments[0].getY() == otherSnake->segments[i].getY()))
            return true;
    }
    return false;
}

bool Snake::hasCollided(Food f)
{
    if( (segments[0].getX() == f.getX()) && (segments[0].getY() == f.getY()))
        return true;
    return false;
}

bool Snake::hasCollided(SnakeSegment flake)
{
    if( (segments[0].getX() == flake.getX()) && (segments[0].getY() == flake.getY()))
        return true;
    return false;
}

bool Snake::hasHitSelf()
{
    for(unsigned int i = 1; i < segments.size(); i++ )
    {
        SnakeSegment s = segments[i];
        if( (segments[0].getX() == s.getX())&&(segments[0].getY() == s.getY()))
            return true;
    }
    return false;
}

bool Snake::hasHitWall()
{
    if( segments[0].getX() > (WIDTH - resolution_/2) || segments[0].getX() < resolution_/2 )
        return true;
    if( segments[0].getY() > (HEIGHT - resolution_/2) || segments[0].getY() < resolution_/2)
        return true;
    return false;
}

int Snake::getStartLength()
{
    return startLength_;
}

int Snake::getResolution()
{
    return resolution_;
}

int Snake::getHeadX()
{
    return headX_;
}

int Snake::getHeadY()
{
    return headY_;
}

int Snake::getHeight()
{
    return height_;
}

SnakeDirection Snake::getDirection()
{
    return direction_;
}

prg::Colour Snake::getColour()
{
    return colour_;
}
