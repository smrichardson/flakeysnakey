#include <stdexcept>
#include <iostream>
#include "../inc/MainState.hpp"
#include "../inc/Input.hpp"
#include "../inc/main.hpp"
#include <prg_interactive.hpp>

int main()
{
    //declare all the game states
    MenuState menu_state;
    MainState main_state;
    PausedState paused_state;
    GameOverState game_over_state;
    try
    {
        //start listening for key presses
        gInput.begin();
        //add all the states to the application
        prg::application.addState( "menu_state", menu_state );
        prg::application.addState( "main_state", main_state );
        prg::application.addState( "paused_state", paused_state );
        prg::application.addState( "game_over_state", game_over_state );
        //set the start state
        prg::application.setState( "menu_state");
        prg::application.setClearColour(prg::Colour::BLACK);
        prg::application.run( WIDTH, HEIGHT, "Sean Richardson", "n3010267");
    }
    catch( std::exception& error )//log any errors
    {
        prg::Log::Message error_message( prg::Log::Message::LT_Error );

        error_message   << "The program has terminated for the following reason: "
                        << error.what();

        prg::logger.add( error_message );
    }

    return 0;
}
