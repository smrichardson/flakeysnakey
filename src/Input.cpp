#include "../inc/Input.hpp"
#include <iostream>
#include <prg/interactive/app.hpp>

Input gInput;

bool Input::onKey( const KeyEvent& ke )
{
    key_states_[ke.key] = ( ke.key_state == KeyEvent::KB_DOWN );
    return true;
}
