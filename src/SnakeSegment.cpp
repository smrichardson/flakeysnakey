#include "../inc/SnakeSegment.hpp"

SnakeSegment::SnakeSegment() {}

SnakeSegment::SnakeSegment(int x, int y)
{
    this->x_ = x;
    this->y_ = y;
}

void SnakeSegment::setX(int x)
{
    this->x_ = x;
}

void SnakeSegment::setY(int y)
{
    this->y_ = y;
}

int SnakeSegment::getX()
{
    return this->x_;
}

int SnakeSegment::getY()
{
    return this->y_;
}

void SnakeSegment::render(prg::Canvas& canvas)
{
    canvas.drawCircle(this->x_, this->y_, 10, prg::Colour::MAGENTA);
}
