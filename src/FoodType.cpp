#include "../inc/FoodType.hpp"

FoodType::FoodType() {}

FoodType::FoodType(const prg::Colour& colour, int foodPoints, int seconds, bool removeTail)
{
    colour_ = colour;
    foodPoints_ = foodPoints;
    timeActive_ = seconds;
    removeTail_ = removeTail;
}

prg::Colour FoodType::getColour()
{
    return colour_;
}

bool FoodType::getRemoveTail()
{
    return removeTail_;
}

int FoodType::getFoodPoints()
{
    return foodPoints_;
}

int FoodType::getTimeActive()
{
    return timeActive_;
}
