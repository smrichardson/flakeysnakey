#include "../inc/AISnake.hpp"
#include "../inc/main.hpp"

//constructor
AISnake::AISnake(int height, int startLength, int resolution) : Snake(height, startLength, resolution)
{
    //set the snake colour
    colour_ = prg::Colour(0,255,255);
}

//updates the snake object
//if the deleteTail is true the last segment is removed when a new segment is added to the front
//if its false the last segment is not removed making the snake grow
//this is used when the snake is eating food
void AISnake::update(bool deleteTail)
{
    //get head of the snake
    SnakeSegment s = segments[0];

    //works out which way the snake is moving
    //works out where the new head of the snake needs to be
    switch(direction_)
    {
    case UP:
        if(s.getY() < HEIGHT - (resolution_/2))
            s.setY(s.getY() + resolution_);
        else
        {
            if(s.getX() > WIDTH/2)
                direction_ = LEFT;
            else
                direction_ = RIGHT;
        }
        break;
    case RIGHT:
        if(s.getX() < WIDTH - (resolution_/2))
            s.setX( s.getX() + resolution_ );
        else
        {
            if(s.getY() > HEIGHT/2)
                direction_ = DOWN;
            else
                direction_ = UP;
        }
        break;
    case DOWN:
        if(s.getY() > resolution_/2)
            s.setY(s.getY() - resolution_);
        else
        {
            if(s.getX() > WIDTH/2)
                direction_ = LEFT;
            else
                direction_ = RIGHT;
        }
        break;
    case LEFT:
        if(s.getX() > resolution_/2)
            s.setX(s.getX() - resolution_);
        else
        {
            if(s.getY() > HEIGHT/2)
                direction_ = DOWN;
            else
                direction_ = UP;
        }
        break;

    }

    //create a new segment at the new position
    SnakeSegment n = SnakeSegment(s.getX(), s.getY());

    //makes sure that the snake can't hit a wall
    if( n.getX() == segments[0].getX() && n.getY() == segments[0].getY())
    {
        if(s.getY() >= HEIGHT - resolution_/2)
        {
            s.setY( HEIGHT - resolution_/2 );
            if(s.getX() > WIDTH/2)
            {
                direction_ = LEFT;
                n.setX( s.getX() - resolution_ );
            }
            else
            {
                direction_ = RIGHT;
                n.setX( s.getX() + resolution_ );
            }
        }
        if(s.getX() >= WIDTH - resolution_/2)
        {
            s.setX( WIDTH - resolution_/2 );
            if(s.getY() > HEIGHT/2)
            {
                direction_ = DOWN;
                n.setY( s.getY() - resolution_);
            }
            else
            {
                direction_ = UP;
                n.setY( s.getY() + resolution_);
            }
        }
        if(s.getY() <= resolution_)
        {
            s.setY( resolution_/2 );
            if(s.getX() < WIDTH/2)
            {
                direction_ = RIGHT;
                n.setX( s.getX() + resolution_ );
            }
            else
            {
                direction_ = LEFT;
                n.setX( s.getX() - resolution_);
            }
        }
        if(s.getX() <= resolution_)
        {
            s.setX( resolution_/2 );
            if(s.getY() < HEIGHT/2)
            {
                direction_ = UP;
                n.setY( s.getY() + resolution_);
            }
            else
            {
                direction_ = DOWN;
                n.setY(s.getY() - resolution_);
            }
        }
    }
    //make the new segment the head of the snake
    segments.push_front(n);

    //remove the end of the new snake to keep the same length
    if(deleteTail)
        segments.pop_back();
}

//limits the new direction of the snake based on the direction it is currently going
void AISnake::setDirection(SnakeDirection d)
{
    switch( d )
    {
    case UP:
        if( direction_ != DOWN)
        {
            if( segments[0].getY() < HEIGHT - resolution_/2)
                direction_ = UP;
            else
            {
                if(segments[0].getX() > WIDTH/2)
                    direction_ = LEFT;
                else
                    direction_ = RIGHT;
            }
        }
        break;
    case RIGHT:
        if ( direction_ != LEFT )
        {
            if( segments[0].getX() < WIDTH - resolution_/2)
                direction_ = RIGHT;
            else
            {
                if(segments[0].getY() > HEIGHT/2)
                    direction_ = DOWN;
                else
                    direction_ = UP;
            }
        }
        break;
    case DOWN:
        if( direction_ != UP )
        {
            if( segments[0].getY() > resolution_/2)
                direction_ = DOWN;
            else
            {
                if(segments[0].getX() > WIDTH/2)
                    direction_ = LEFT;
                else
                    direction_ = RIGHT;
            }
        }
        break;
    case LEFT:
        if( direction_ != RIGHT )
        {
            if( segments[0].getX() > resolution_/2)
                direction_ = LEFT;
            else
            {
                if(segments[0].getY() > HEIGHT/2)
                    direction_ = DOWN;
                else
                    direction_ = UP;
            }
        }
        break;
    }
}
