#include "../inc/Food.hpp"
#include"../inc/FoodType.hpp"
#include <time.h>

Food::Food() {}

Food::Food(int x, int y, float radius, FoodType foodType)
{
    x_ = x;
    y_ = y;
    foodType_ = foodType;
    timeCreated_ = time(0);
    radius_ = radius;
}

//makes sure the food doesnt stay on the board for too long
//different food types stay on the board for different lengths of time
bool Food::update()
{
    double seconds = difftime( time(0), timeCreated_ );
    if( seconds >= foodType_.getTimeActive())
        return true;

    return false;
}

//draws the food to the board
void Food::render(prg::Canvas& canvas)
{
    canvas.drawCircle(
        x_, y_,
        radius_,
        foodType_.getColour()
    );
}

int Food::getX()
{
    return x_;
}

int Food::getY()
{
    return y_;
}

float Food::getRadius()
{
    return radius_;
}

FoodType Food::getFoodType()
{
    return foodType_;
}
