#include "../inc/MenuState.hpp"
#include "../inc/main.hpp"

//messages
const char* title = "Flakey Snakey";
const char* message = "press any key to play";
const char* pauseMessage = "press space to pause";
const char* controls = "controls - WASD";

MenuState::MenuState()
{

}

bool MenuState::onCreate()
{
    return true;
}
bool MenuState::onDestroy()
{

    return true;
}
void MenuState::onEntry()
{
    prg::application.addKeyListener( *this );
}
void MenuState::onExit()
{
    prg::application.removeKeyListener( *this );
}
void MenuState::onUpdate()
{

}
void MenuState::onRender(prg::Canvas& canvas)
{
    prg::uint dimensions[2];
    titleFont_.computePrintDimensions(
        dimensions,
        title
    );

    prg::uint   center_x { prg::application.getScreenWidth() / 2 - dimensions[0] / 2 },
        center_y { prg::application.getScreenHeight() / 2 - dimensions[1] / 2 };

    titleFont_.print(
        canvas,
        center_x,
        center_y + 75,
        prg::Colour::WHITE,
        title
    );
    messageFont_.print(
        canvas,
        center_x + 115,
        center_y - 25,
        prg::Colour::WHITE,
        message
    );
    messageFont_.print(
        canvas,
        center_x + 115,
        center_y - 50,
        prg::Colour::WHITE,
        pauseMessage
    );
    messageFont_.print(
        canvas,
        center_x + 160,
        center_y - 75,
        prg::Colour::WHITE,
        controls
    );
}
void MenuState::onTimer( prg::Timer& timer )
{

}

bool MenuState::onKey( const KeyEvent& key_event )
{
    prg::application.setState("main_state");
    return true;
}
