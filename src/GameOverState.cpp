#include "../inc/GameOverState.hpp"

//access the global variables for the playerScore and the aiScore
int aScore;
int pScore;

//messages displayed on the screen
const char* playerWinMessage = "You Win!";
const char* aiWinMessage = "You Lose!";
const char* drawMessage = "Draw!";
const char* replayMessage = "press enter to replay";
const char* exitMessage = "press esc to exit";

GameOverState::GameOverState()
{

}

bool GameOverState::onCreate()
{
    return true;
}

bool GameOverState::onDestroy()
{
    return true;
}

void GameOverState::onEntry()
{
    //only listen for key presses in this state when the state is entered
    prg::application.addKeyListener( *this );
}

void GameOverState::onExit()
{
    //stop listening for key presses when state is left
    prg::application.removeKeyListener( *this );
}

void GameOverState::onUpdate()
{

}

void GameOverState::onRender(prg::Canvas& canvas)
{
    //working out how big the font needs to be depending on the screen size
    prg::uint dimensions[2];
    playerWinMessage_.computePrintDimensions(
        dimensions,
        playerWinMessage
    );
    prg::uint   center_x { prg::application.getScreenWidth() / 2 - dimensions[0] / 2 },
        center_y { prg::application.getScreenHeight() / 2 - dimensions[1] / 2 };

    //display correct message for the winner of the game
    if(aScore < pScore)
    {
        playerWinMessage_.print(
            canvas,
            center_x,
            center_y + 75,
            prg::Colour::WHITE,
            playerWinMessage
        );
    }
    else if(aScore > pScore)
        playerWinMessage_.print(
            canvas,
            center_x - 50,
            center_y + 75,
            prg::Colour::WHITE,
            aiWinMessage
        );
    else if(aScore == pScore)
        playerWinMessage_.print(
            canvas,
            center_x + 50,
            center_y + 75,
            prg::Colour::WHITE,
            drawMessage
        );
    replayMessage_.print(
        canvas,
        center_x,
        center_y - 50,
        prg::Colour::WHITE,
        replayMessage
    );
    replayMessage_.print(
        canvas,
        center_x + 40,
        center_y - 75,
        prg::Colour::WHITE,
        exitMessage
    );

}

void GameOverState::onTimer( prg::Timer& timer )
{

}

//handles key presses
bool GameOverState::onKey( const KeyEvent& key_event )
{
    switch(key_event.key_state)
    {
    case KeyEvent::KB_DOWN:
        switch( key_event.key )
        {
        case 27://exit on escape key
            prg::application.exit();
            break;
        case 13://go back to MainState on return key
            prg::application.setState("main_state");
            break;
        }
        break;
    }
    return true;
}
