#ifndef GAMEOVERSTATE_HPP_INCLUDED
#define GAMEOVERSTATE_HPP_INCLUDED
#include <prg_interactive.hpp>
#include "../inc/main.hpp"

/*
    a state which shows when the game is over and who the winner is
*/

class GameOverState : public prg::IAppState, public prg::ITimerEvent, public prg::IKeyEvent
{
public:
    GameOverState();
    bool onCreate() override;
    bool onDestroy() override;
    void onEntry() override;
    void onExit() override;
    void onUpdate() override;
    void onRender(prg::Canvas& canvas) override;
    void onTimer( prg::Timer& timer ) override;
private:
    bool onKey( const KeyEvent& key_event ) override;//handles key presses for the state
    prg::Font playerWinMessage_ { "resources/fonts/ka1.ttf", 32 }; //declares a font
    prg::Font replayMessage_ { "resources/fonts/ka1.ttf", 12 }; //declares a font
};

#endif // GAMEOVERSTATE_HPP_INCLUDED
