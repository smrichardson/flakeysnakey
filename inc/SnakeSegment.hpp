#if !defined(_SNAKESEGMENT_H)
#define _SNAKESEGMENT_H
#include <prg_interactive.hpp>

/*
    represents an individual snake segment
*/

class SnakeSegment
{
public:
    SnakeSegment();
    SnakeSegment(int x, int y);
    void render(prg::Canvas& canvas);

    //setters
    void setX(int x);
    void setY(int y);

    //getters
    int getX();
    int getY();
private:
    int x_;//the x co-ordinate of the segment
    int y_;//the y co-ordinate of the segment
};

#endif  //_SNAKESEGMENT_H
