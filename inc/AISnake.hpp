#if !defined(_AISNAKE_H)
#define _AISNAKE_H

#include "Snake.hpp"

/*
    represents the snake which is controlled by artificial intelligence
*/

class AISnake : public Snake
{
public:
    AISnake(int height, int startLength, int resolution);
    void update(bool deleteTail);
    void setDirection(SnakeDirection direction);
};

#endif  //_AISNAKE_H
