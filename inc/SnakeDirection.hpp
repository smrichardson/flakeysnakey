#ifndef SNAKEDIRECTION_HPP_INCLUDED
#define SNAKEDIRECTION_HPP_INCLUDED

/*
    describes the different direction a snake can travel
*/

enum SnakeDirection
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};

#endif // SNAKEDIRECTION_HPP_INCLUDED
