#if !defined(_PLAYERSNAKE_H)
#define _PLAYERSNAKE_H

#include "Snake.hpp"
#include "Input.hpp"

/*
    represents the snake which is controlled by a player
*/

class PlayerSnake : public Snake, public prg::IKeyEvent
{
public:
    PlayerSnake(int height, int startLength, int resolution);
    virtual ~PlayerSnake();
    void update(bool deleteTail);
    bool onKey( const KeyEvent& key_event ) override;
};

#endif  //_PLAYERSNAKE_H
