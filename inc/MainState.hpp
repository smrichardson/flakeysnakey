#if !defined(_MAINSTATE_H)
#define _MAINSTATE_H

#include <prg_interactive.hpp>
#include "Food.hpp"
#include "Snake.hpp"
#include "AISnake.hpp"
#include "PlayerSnake.hpp"
#include "FoodType.hpp"
#include <list>
#include<string>
#include<vector>
#include "MenuState.hpp"
#include "PausedState.hpp"
#include "GameOverState.hpp"

/*
    the state which handles the playing of the game
*/

class MainState : public prg::IAppState, public prg::ITimerEvent, public prg::IKeyEvent
{
public:

    MainState();
    bool onCreate() override;
    bool onDestroy() override;
    void onEntry() override;
    void onExit() override;
    void onUpdate() override;
    void onRender(prg::Canvas& canvas) override;
    void onTimer( prg::Timer& timer ) override;

private:
    int playerScore = 0;//number of points the player has gained
    int aiScore = 0;//number of points the AI has gained
    int updateCount = 0;//a counter which is used to slow down the AI Snake
    int xmax;//the maximum x co-ordinate the food can be spawned at
    int ymax;//the maximum y co-ordinate the food can be spawned at
    int xmin;//the minimum x co-ordinate the food can be spawned at
    int ymin;//the minimum y co-ordinate the food can be spawned at
    long double lastUpdatedTime;//the last time the onUpdate method ran in the state
    std::list<Food> foods;//a list which holds all of the current foods
    std::vector<FoodType> foodTypes;//holds all of the current food types
    std::vector<SnakeSegment> flakes;//holds the flakes of the snakes
    PlayerSnake* playerSnake;//references the playerSnake object
    AISnake* aiSnake;//references the aiSnake object
    prg::Timer aiSnakeTimer_ {1, 75, *this };//this is used to drive the aiSnake updates

    SnakeDirection calculateDirection(Snake* s, Food f);//works out the direction the snake has to travel to get that food
    std::string convertInt(int number);//conversion
    bool onKey( const KeyEvent& key_event ) override;//key handler for the state
    void addFood();//adds a new food to foods
    float foodDistance(Snake* s, Food f);//works out the distance between a snake and a food
    int randomEvenNumber(int minimum, int maximum);//generates a random even number between a minimum and a maximum
    int randomOddNumber(int minimum, int maximum);//generates a random odd number between a minimum and a maximum
    int randomNumber(int minimum, int maximum);//generates a random number between a minimum and a maximum
    void pauseGame();//pauses the game
    void gameOver();//ends the game
    void createFlake(Snake* s);//breaks a flake off the snake

};

#endif  //_MAINSTATE_H
