#if !defined    INPUT_HPP
#define         INPUT_HPP

#include <prg_interactive.hpp>
#include <prg/interactive/app_state.hpp>

/*
    a class which handles keyboard input
    stolen from example#1 code
*/

class Input : public prg::IKeyEvent
{
public:

    ~Input()
    {
        end();
    }

    void begin()
    {
        prg::application.addKeyListener( *this );
    }

    void end()
    {
        prg::application.removeKeyListener( *this );
    }

    bool onKey( const KeyEvent& ke ) override;
    bool getKey( KeyEvent::EKeyInfo key ) const
    {
        return key_states_[key];
    }
    bool getKey( unsigned char key ) const
    {
        return key_states_[key];
    }

private:
    bool key_states_[512] { false };
};

extern Input gInput;

#endif // INPUT_HPP
