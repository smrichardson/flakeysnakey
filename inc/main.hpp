#ifndef MAIN_HPP_INCLUDED
#define MAIN_HPP_INCLUDED

/*
    system wide definitions
*/
#define WIDTH 800
#define HEIGHT 600
#define RED_FOOD 0
#define GREEN_FOOD 1
#define YELLOW_FOOD 2

/*
    global variables
    it is needed to pass data between states
    would have used prg::application.setData() if available in the version I had
*/
extern int aScore;
extern int pScore;

#endif // MAIN_HPP_INCLUDED
