#ifndef PAUSEDSTATE_HPP_INCLUDED
#define PAUSEDSTATE_HPP_INCLUDED
#include <prg_interactive.hpp>
#include "../inc/AISnake.hpp"

class PausedState : public prg::IAppState, public prg::ITimerEvent, public prg::IKeyEvent
{
public:
    PausedState();
    bool onCreate() override;
    bool onDestroy() override;
    void onEntry() override;
    void onExit() override;
    void onUpdate() override;
    void onRender(prg::Canvas& canvas) override;
    void onTimer( prg::Timer& timer ) override;
private:
    prg::Font   pausedFont_ { "resources/fonts/ka1.ttf", 12 }; //create a font
    bool onKey( const KeyEvent& key_event) override;//key handler for state
    AISnake* demoSnake;//references a demoSnake for display on the pause screen
    prg::Timer demoSnakeTimer_ {2, 75, *this };//handles the update for the demoSnake
    void resumeGame();//resumes the game
};
#endif // PAUSEDSTATE_HPP_INCLUDED
