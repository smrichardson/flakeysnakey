#include <iostream>
#include <prg_interactive.hpp>

#if !defined(_FOODTYPE_H)
#define _FOODTYPE_H

/*
Describes the different types of food that are available
*/
class FoodType
{
public:
    FoodType();
    FoodType(const prg::Colour& colour, int foodPoints, int seconds, bool removeTail);

    //getters
    int getTimeActive();
    bool getRemoveTail();
    int getFoodPoints();
    prg::Colour getColour();
private:
    int timeActive_; // the length of time (in seconds) that the food type is active for
    bool removeTail_; // denotes whether a segment flakes off from the snake when this food type is eaten
    int foodPoints_; // the number of points associated with the food type
    prg::Colour colour_; // the colour associated with the food type
};

#endif  //_FOODTYPE_H
