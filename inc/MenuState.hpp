#ifndef MENUSTATE_HPP_INCLUDED
#define MENUSTATE_HPP_INCLUDED
#include <prg_interactive.hpp>

/*
    handles the start-up screen
*/
class MenuState : public prg::IAppState, public prg::ITimerEvent, public prg::IKeyEvent
{
public:
    MenuState();
    bool onCreate() override;
    bool onDestroy() override;
    void onEntry() override;
    void onExit() override;
    void onUpdate() override;
    void onRender(prg::Canvas& canvas) override;
    void onTimer( prg::Timer& timer ) override;
private:
    prg::Font   titleFont_ { "resources/fonts/ka1.ttf", 32 }; //creates a font
    prg::Font   messageFont_ { "resources/fonts/ka1.ttf", 12 };//creates a font
    bool onKey( const KeyEvent& key_event ) override;//key handler for the state
};

#endif // MENUSTATE_HPP_INCLUDED
