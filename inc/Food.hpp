#include <prg_interactive.hpp>

#if !defined(_FOOD_H)
#define _FOOD_H

#include "FoodType.hpp"

/*
    represents a particular foodtype at a particular location on the screen
*/

class Food
{
public:
    Food();
    Food(int x, int y, float radius, FoodType foodType);
    bool update();
    void render(prg::Canvas& canvas);

    //getters
    int getX();
    int getY();
    float getRadius();
    FoodType getFoodType();
private:
    time_t timeCreated_;//holds the time when the food is created
    int x_;//the x co-ordinate of the food on the screen
    int y_;//the y co-ordinate of the food on the screen
    float radius_;//the radius of the displayed food
    FoodType foodType_;//the type of the displayed food
};

#endif  //_FOOD_H
