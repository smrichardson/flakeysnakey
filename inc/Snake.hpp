#if !defined(_SNAKE_H)
#define _SNAKE_H

#include "Food.hpp"
#include "SnakeSegment.hpp"
#include "SnakeDirection.hpp"
#include <prg_interactive.hpp>
#include <deque>
#include <vector>

/*
    represents a generic snake object
*/

class Snake
{
public:
    std::deque<SnakeSegment> segments;//holds the snake segments
    Snake();
    Snake(int height, int startLength, int resolution);
    void render( prg::Canvas& canvas);
    void reset();//resets the snake to its starting conditions
    bool hasCollided(Food f);//checks if the snake has collided with any food
    bool hasCollided(Snake* s);//checks if the snake has collided with any other snake
    bool hasCollided(SnakeSegment flake);//checks if the snake has collided with a flake
    bool hasHitSelf();//checks if the snake has hit itself
    bool hasHitWall();//checks if the snake has hit a wall

    //getters
    int getStartLength();
    int getResolution();
    int getHeadX();
    int getHeadY();
    int getHeight();
    SnakeDirection getDirection();
    prg::Colour getColour();
protected:
    prg::Colour colour_;//holds the colour for a snake
    SnakeDirection direction_;//holds the current direction of a snake
    int resolution_;//the diameter of each snakeSegment
private:
    int startLength_;//the starting number of segments in a snake
    int headX_;//the starting x co-ordinate of a snake head
    int headY_;//the starting y co-ordinate of a snake head
    int height_;//the starting height up the board of a snake

};

#endif  //_SNAKE_H
